# C++ example project scanned on SonarCloud using Gitlab 

[![Build status](https://gitlab.com/sonarsource/sonarcloud/sonarcloud_example_cpp-cmake-linux-gitlab/badges/master/pipeline.svg)](https://gitlab-ci.org/SonarSource/sonarcloud_example_cpp-cmake-linux-gitlab)[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sonarcloud_example_cpp-cmake-linux-gitlab&metric=alert_status)](https://sonarcloud.io/dashboard?id=sonarcloud_example_cpp-cmake-linux-gitlab)

#### This project is analysed on [SonarCloud](https://sonarcloud.io)!

To run an analysis on a C/C++/Objective-C project and push it to SonarCloud:

1. Create a `sonar-project.properties` files to store your configuration
2. In your `.gitlab-ci.yml` file:
   1. Download the *build-wrapper* and the *sonar-scanner*
   2. Wrap your compilation with the *build-wrapper*
   3. Run the *sonar-scanner* later on

You can take a look at the
[sonar-project.properties](https://gitlab.com/sonarsource/sonarcloud/sonarcloud_example_cpp-cmake-linux-gitlab/-/blob/master/sonar-project.properties)
and
[.gitlab-ci.yml](https://gitlab.com/sonarsource/sonarcloud/sonarcloud_example_cpp-cmake-linux-gitlab/-/blob/master/.gitlab-ci.yml)
files of this project to see it in practice.

## Links
- [Documentation of the C/C++/Objective-C plugin and its with Build Wrapper](http://docs.sonarqube.org/x/pwAv)
